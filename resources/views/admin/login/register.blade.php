@extends('admin.layouts.gentelella.login')
@section('content')

    <div class="">
        <div id="wrapper">
            <div id="register" class="animate form">
                <section class="login_content">
                    <form method="POST" action="/admin/register">
                        {!! csrf_field() !!}
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" name="username" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" name="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <input class="btn btn-default submit" type="submit" name="submit" value="Submit" />
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="/admin/login" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

@stop