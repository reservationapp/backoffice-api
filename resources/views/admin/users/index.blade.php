@extends('admin.layouts.gentelella.admin')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Users Listing</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Users</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/admin/users/create">Add New</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <!-- start users list -->
                        <table class="table table-striped users">
                            <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th style="width: 20%">User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                foreach ($users as $i => $v):
                            ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><a><?php echo (isset($v['firstname']) && isset($v['lastname'])) ? ($v['firstname'] . ' ' . $v ['lastname'] ) : ''; ?></a><br /><small>Created at: <?php echo date('Y-M-d h:i:s', $v['created_at']->sec); ?></small></td>
                                    <td><?php echo isset($v['email']) ? $v['email'] : ''; ?></td>
                                    <td><?php echo isset($v['gender']) ? $v['gender'] : ''; ?></td>
                                    <td><?php echo isset($v['role']) ? $v['role'] : 'user'; ?></td>
                                    <td>Active</td>
                                    <td>
                                        <a href="/admin/users/<?php echo $v['_id']->{'$id'}; ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                                        <a href="/admin/users/<?php echo $v['_id']->{'$id'}; ?>/edit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="/admin/users/<?php echo $v['_id']->{'$id'}; ?>/destroy" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                            <?php
                                endforeach;
                            ?>
                                
                            </tbody>
                        </table>
                        <!-- end users list -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop