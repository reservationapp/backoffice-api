<?php

use Illuminate\Database\Seeder;

class ReservationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->truncate();
        $faker = Faker\Factory::create();
        $services = \App\Service::all();
        $users = \App\User::where('role','user')->get()->lists('id');
        $data = [];
        foreach($users as $user){
            foreach($services->random(3) as $service){
                $rDays = random_int(1,60);
                $rUnit = random_int(1,8);
                $data[] = [
                    'user_id' => $user,
                    'service_id' => $service->id,
                    'date' => \Carbon\Carbon::createFromFormat('Y-m-d',$service->start_date)->add(new DateInterval('P'.$rDays.'D'))->format('Y-m-d'),
                    'start_time' => $service->start_time + ($rUnit * $service->unit),
                    'end_time' => $service->start_time + ($rUnit * $service->unit) + $service->unit,
                ];
            }
        }
        \App\Reservation::unguard();
        \App\Reservation::insert($data);
        \App\Reservation::reguard();
    }
}
