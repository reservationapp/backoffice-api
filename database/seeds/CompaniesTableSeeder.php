<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->truncate();
        $faker = Faker\Factory::create();
        $users = \App\User::where('role','company')->get(['id']);
        $data = [];
        foreach($users as $user){
            $data[] = [
                'name' => $faker->company,
                'user_id' => $user->id,
                'cui' => $faker->randomNumber(7),
            ];
        }
        \App\Company::unguard();
        \App\Company::insert($data);
        \App\Company::reguard();
    }
}
