<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        $data = [
            [
                'key'=> 'admin',
                'name'=> 'Admin',
                'description'=> 'Admin'
            ],
            [
                'key'=> 'company',
                'name'=> 'Company',
                'description'=> 'Company'
            ],
            [
                'key'=> 'user',
                'name'=> 'User',
                'description'=> 'Simple user'
            ],
        ];
        \App\Role::unguard();
        \App\Role::insert($data);
        \App\Role::reguard();
    }
}
