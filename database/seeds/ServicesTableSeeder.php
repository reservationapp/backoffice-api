<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->truncate();
        $faker = Faker\Factory::create();
        $companies = \App\Company::all();
        $data = [];
        foreach ($companies as $company) {
            $data[] = [
                'name' => $faker->name,
                'company_id' => $company->id,
                'start_date' => \Carbon\Carbon::now()->format('Y-m-d'),
                'end_date' => \Carbon\Carbon::now()->add(new DateInterval('P1Y'))->format('Y-m-d'),
                'start_time' => $faker->randomElement([7, 8, 9]) * 60,
                'end_time' => $faker->randomElement([14, 15, 16, 17, 20]) * 60,
                'unit' => $faker->randomElement([30, 50, 60]),
                'price' => $faker->randomNumber(2),
                'currency' => $faker->randomElement(['RON', 'EUR', 'USD'])
            ];
        }
        \App\Service::unguard();
        \App\Service::insert($data);
        \App\Service::reguard();
    }
}
