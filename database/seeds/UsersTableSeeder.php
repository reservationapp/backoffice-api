<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        factory(App\User::class, 100)->create();
        factory(App\User::class, 'admin', 5)->create();
        factory(App\User::class, 'company', 50)->create();
    }
}
