<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'email' => strtolower($faker->unique()->email),
        'password' => bcrypt('admin'),
        'username' => strtolower($faker->unique()->userName),
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'phone' => $faker->phoneNumber,
        'gender' => $faker->randomElement(['m','f']),
        'birthdate' => $faker->date('Y-m-d'),
        'account_confirmed' => $faker->randomElement([1,0]),
        'status' => 'approved',
        'remember_token' => str_random(10),
        'role' => 'user'
    ];
});

$factory->defineAs(App\User::class, 'admin', function ($faker) use ($factory) {
    $user = $factory->raw(App\User::class);
    return array_merge($user, [
        'role' => 'admin',
        'email' => 'admin.' . $user['email'],
        'username' => 'admin.' . $user['username']
    ]);
});

$factory->defineAs(App\User::class, 'company', function ($faker) use ($factory) {
    $user = $factory->raw(App\User::class);
    return array_merge($user, [
        'role' => 'company',
        'email' => 'company.' . $user['email'],
        'username' => 'company.' . $user['username']
    ]);
});
