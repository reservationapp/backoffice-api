<?php

namespace App\Services;

use EllipseSynergie\ApiResponse\Laravel\Response;

class ApiResponse extends Response
{

    public function withError($message, $errorCode, $exception = '')
    {
        $response = [
            'error' => [
                'code' => $errorCode,
                'http_code' => $this->statusCode,
                'message' => $message
            ]
        ];

        if (\Config::get('songrow.app_detailed_exception', false)) {
            $response['error']['exception'] = @(string)$exception;
        }

        return $this->withArray($response);
    }
}
