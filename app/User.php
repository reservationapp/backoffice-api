<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Model;

/**
 * Class User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @package App\Models
 */
class User extends Model implements AuthContract
{
    use Authenticatable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'firstname', 'lastname', 'email', 'password', 'gender', 'birthdate', 'role'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
