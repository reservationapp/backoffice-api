<?php


namespace App\Transformers;

use Faker\Factory;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class BusinessTransformer extends TransformerAbstract
{


    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'image'
    ];

    public function transform()
    {
        $faker = new Factory();
        $faker = $faker->create();
        $array = [
            'id' => $faker->numberBetween(1,1000),
            'name' => $faker->company,
            'address' => $faker->address,
            'image' => $faker->imageUrl('200','300')
        ];
        return $array;
    }

    public function includeImage(){
        $a = new Collection();
        $a->push('1');
        $a->push('1');
        $a->push('1');
        return $this->collection($a, new ImageTransformer(),null,null,['source'=>'Google']);
    }
}