<?php
/**
 * Created by PhpStorm.
 * User: lorand.gombos
 * Date: 2/3/2016
 * Time: 3:21 PM
 */

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $data = [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'email' => $user->email
        ];
        return $data;
    }
}
