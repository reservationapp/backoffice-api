<?php

namespace App;

use Jenssegers\Mongodb\Model;

class Role extends Model
{
    protected $fillable = [
        'key', 'name', 'description',
    ];
}
