<?php
Route::group(array('namespace'=>'Admin'), function()
{
    Route::get('/admin/', array('as' => 'admin', 'uses' => 'AdminController@index'));
    Route::get('/admin/login', array('as' => 'index', 'uses' => 'LoginController@index'));
    Route::get('/admin/register', array('as' => 'register', 'uses' => 'LoginController@register'));
    Route::post('/admin/register', array('as' => 'register', 'uses' => 'LoginController@register'));
    Route::get('/admin/lostpassword', array('as' => 'lostpassword', 'uses' => 'LoginController@lostpassword'));
    Route::post('/admin/users/create', array('as' => 'user_create', 'uses' => 'UsersController@create'));
    Route::post('/admin/users/{id}/edit', array('as' => 'user_edit', 'uses' => 'UsersController@edit'));
    Route::post('/admin/companies/create', array('as' => 'company_create', 'uses' => 'CompaniesController@create'));
    Route::post('/admin/companies/{id}/edit', array('as' => 'company_edit', 'uses' => 'CompaniesController@edit'));
    Route::post('/admin/services/create', array('as' => 'service_create', 'uses' => 'ServicesController@create'));
    Route::post('/admin/services/{id}/edit', array('as' => 'service_edit', 'uses' => 'ServicesController@edit'));
    Route::post('/admin/reservations/create', array('as' => 'reservation_create', 'uses' => 'ReservationsController@create'));
    Route::post('/admin/reservations/{id}/edit', array('as' => 'reservation_edit', 'uses' => 'ReservationsController@edit'));
    Route::get('/admin/reports', array('as' => 'report', 'uses' => 'ReportsController@index'));
});

Route::resource('/admin/users', 'Admin\UsersController');
Route::resource('/admin/companies', 'Admin\CompaniesController');
Route::resource('/admin/services', 'Admin\ServicesController');
Route::resource('/admin/reservations', 'Admin\ReservationsController');