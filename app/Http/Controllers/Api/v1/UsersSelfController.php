<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Api\UsersSelfStore;
use App\Transformers\UserTransformer;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

class UsersSelfController extends ApiController
{
    /**
     * @SWG\Get(path="/users/self",
     *      tags={"UsersSelf"},
     *      summary="Get authenticated user",
     *      description="",
     *      operationId="authUser",
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function index()
    {
        return $this->response->withItem(Auth::user(), new UserTransformer());
    }

    /**
     * @SWG\Post(path="/users/self",
     *      tags={"UsersSelf"},
     *      summary="Update the current user",
     *      description="",
     *      operationId="storeSelfUSer",
     *      @SWG\Parameter(
     *          in="formData",
     *          name="email",
     *          description="User e-mail",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          in="formData",
     *          name="profile_image",
     *          description="User profile image",
     *          required=false,
     *          type="file"
     *      ),
     *      @SWG\Response(response="default", description="successful operation")
     *  )
     * @param UsersSelfStore|Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function store(UsersSelfStore $request)
    {

    }
}
