<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Transformers\BusinessTransformer;
use Illuminate\Support\Collection;

class BusinessController extends ApiController
{

    public function index(){
        $result = new Collection();
        $result->push('1');
        $result->push('2');
        $result->push('3');
        $result->push('3');
        $result->push('3');
        $result->push('3');
        $result->push('3');
        return $this->response->withCollection($result, new BusinessTransformer(),null, null,['voicu'=>'Tibea']);
    }

}