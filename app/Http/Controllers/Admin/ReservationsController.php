<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\BusinessTransformer;
use Illuminate\Support\Collection;

class ReservationsController extends Controller
{
    public function index(){
        return \View::make('admin.admin.index');
    }
    
    public function create(Request $request){
        return \View::make('admin.admin.index');
    }
    
    public function edit(Request $request, $id){
        return \View::make('admin.admin.index');
    }
}