<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller; // using controller class

class AdminController extends Controller {
    public function index() {
        return \View::make('admin.admin.index');
    }
    
}