<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\BusinessTransformer;
use Illuminate\Support\Collection;

class UsersController extends Controller
{

    /**
     * 
     * @return type
     */
    public function index(){
        $users = DB::table('users')->get();
        
        return \View::make('admin.users.index', array('users' => $users));
    }
    
    /**
     * 
     * @return type
     */
    public function create(Request $request){
        
        $data = array();
        if ($request->isMethod('post')) {
            
            $username  = $request->input('username');
            $firstname  = $request->input('firstname');
            $lastname  = $request->input('lastname');
            $email  = $request->input('email');
            $password  = $request->input('password');
            $gender  = $request->input('gender');
            $birthdate  = $request->input('birthdate');
            $role  = $request->input('role');
            
            User::create([
                'username' => $username,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'password' => bcrypt($password),
                'gender' => $gender,
                'birthdate' => $birthdate,
                'role' => $role,
            ]);
        }
        
        $data['roles'] = DB::table('roles')->get();
        $data['formUrl'] = '/admin/users/create';
                
        return \View::make('admin.users.create', $data);
    }
    
    /**
     * 
     * @return type
     */
    public function edit(Request $request, $id){
        $data = array();
        if ($request->isMethod('post')) {
            
            $username  = $request->input('username');
            $firstname  = $request->input('firstname');
            $lastname  = $request->input('lastname');
            $email  = $request->input('email');
            $password  = $request->input('password');
            $gender  = $request->input('gender');
            $birthdate  = $request->input('birthdate');
            $role  = $request->input('role');
            
            User::where('_id', $id)->update([
                'username' => $username,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'password' => bcrypt($password),
                'gender' => $gender,
                'birthdate' => $birthdate,
                'role' => $role,
            ]);
        }
        
        $data['roles'] = DB::table('roles')->get();
        $data['user'] = DB::table('users')->where('_id', $id)->first();
        $data['formUrl'] = '/admin/users/' . $id . '/edit';
        
        return \View::make('admin.users.create', $data);
    }
}