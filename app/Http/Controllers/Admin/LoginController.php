<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller; // using controller class
use Illuminate\Http\Request;
use App\User;
use Validator;

class LoginController extends Controller {
    public function index(Request $request) {
        return \View::make('admin.login.login');
    }
    
    public function register(Request $request) {
        
        if ($request->isMethod('post')) {
            $username = $request->input('username');
            $email = $request->input('email');
            $password = $request->input('password');

            return User::create([
                'username' => $username,
                'email' => $email,
                'password' => bcrypt($password),
            ]);
        }
        
        return \View::make('admin.login.register');
    }
    
    public function lostpassword(Request $request) {
        return \View::make('admin.login.lostpassword');
    }
}