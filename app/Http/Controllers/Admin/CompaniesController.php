<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\BusinessTransformer;
use Illuminate\Support\Collection;

class CompaniesController extends Controller {

    public function index() {
        $data = array();
        $data['companies'] = DB::table('companies')->get();

        return \View::make('admin.companies.index', $data);
    }

    public function create(Request $request) {
        return \View::make('admin.admin.index');
    }

    public function edit(Request $request, $id) {
        return \View::make('admin.admin.index');
    }

}
