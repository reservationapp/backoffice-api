<?php
/**
 * Created by PhpStorm.
 * User: lorand.gombos
 * Date: 1/30/2016
 * Time: 12:24 PM
 */

namespace App\Http\Controllers;

use App\Services\ApiResponse;
use EllipseSynergie\ApiResponse\Contracts\Response;

class ApiController extends Controller
{
    /**
     * @var ApiResponse
     */
    public $response;

    /**
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
}