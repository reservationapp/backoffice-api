<?php
Route::post('auth', 'AuthController@index');
Route::group(['middleware' => ['jwt.refresh', 'jwt.auth']], function () {
    Route::resource('users/self', 'UsersSelfController');
});
Route::get('auth/user', 'AuthController@get_user');