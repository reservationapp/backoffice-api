<?php

namespace App\Exceptions;

use App\Services\ApiResponse;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Fractal\Manager;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($request->is('api/*') ){
            return $this->apiRender($request,$e);
        }
        return parent::render($request, $e);
    }

    public function apiRender($request, Exception $e){
        if($e instanceof HttpException){
            $code = $e->getCode();
            $statusCode = $e->getStatusCode();
            $message = $e->getMessage();
        } else {
            $statusCode = $code = $e->getCode();
            if($code < 100 || $code >= 600){
                $statusCode = 501;
            }
            $message = $e->getMessage();
        }
        $response = new ApiResponse(new Manager());
        return $response->setStatusCode($statusCode)->withError($message,$code,$e);
    }
}
